GAWHS-SciOly 2013-2014 Season
============

Science Olympiad Construction Trials

List of events to be coded:
1. Mission Possible
2. Scrambler
3. Compound Machines
4. Maglev

Features of the program:
1. Tier classification
2. Scores
